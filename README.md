# zyplayer-data-screen

#### 介绍
数据大屏数据查询处理端，具有数据源保存、组件保存、组件数据查询等功能，本项目没有前端、登录及图片保存等功能，需配合 http://datascreen.zyplayer.com 网站（以下简称‘网站’）使用。
出于网站规划，暂未对基本功能及前端开源，为了让用户放心使用，故将数据源存储、数据查询抽离为本项目。

#### 软件架构
本项目代码由JAVA代码编写，使用SpringBoot、MyBatis-Plus框架，MySQL数据源、Druid连接池

#### 使用教程
1. clone本项目>`git clone https://gitee.com/zyplayer/zyplayer-doc.git`
2. 新建数据库：zyplayer_data_screen
3. 执行建表语句：zyplayer-data-screen/src/resources/sql/zyplayer_data_screen.x.x.x.sql
4. 修改数据源及账号密码：zyplayer-data-screen/src/resources/application-dev.yml
5. 启动项目：com.zyplayer.data.screen.Application#main()
6. 访问 http://datascreen.zyplayer.com 网站，微信扫码登录，从右上角进入个人中心
7. 编辑‘自建服务器地址’，输入：http://127.0.0.1:8090 点击‘确定’即可。
如果是第二次添加，或者有多个用户添加此服务地址，则需要填写：服务器公钥。
该公钥存放于数据库表：private_key 字段：public_key，第一次接入时会自动创建一条记录。
至此，服务已搭建完成，此时在“数据源管理”处保存的数据源将直接保存在您自建的服务器里，大屏组件请求数据的接口也将请求此服务。
需要注意的是：如果创建的大屏想要分享给公网可访问，需要将此服务部署在公网上。
8. clone到本地后可自由修改，建议定期同步仓库代码，以保证需求同步、BUG处理。

#### 界面展示
http://datascreen.zyplayer.com/#/screen/view?code=f82ffc01db0b4d138ffd99a6a7640e60


#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

