package com.zyplayer.data.screen.framework.configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.zyplayer.data.screen.framework.db.bean.DatabaseFactoryBean;
import com.zyplayer.data.screen.framework.db.executor.DatasourceUtil;
import com.zyplayer.data.screen.repository.manage.entity.DbDatasource;
import com.zyplayer.data.screen.repository.support.interceptor.SqlLogInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * mybatis plus数据库配置
 */
@Configuration
public class MybatisPlusConfig {
	
	/**
	 * sql日志
	 **/
	private static final SqlLogInterceptor SQL_LOG_INTERCEPTOR;
	
	static {
		{
			SQL_LOG_INTERCEPTOR = new SqlLogInterceptor();
			Properties properties = new Properties();
			SQL_LOG_INTERCEPTOR.setProperties(properties);
		}
	}
	
	/**
	 * 数据库配置
	 */
	@Configuration
	@EnableTransactionManagement
	@MapperScan(value = "com.zyplayer.data.screen.repository.manage.mapper", sqlSessionFactoryRef = "manageSqlSessionFactory")
	static class ManageMybatisDbConfig {
		
		@Value("${zyplayer.data.admin.datasource.driverClassName}")
		private String driverClassName;
		@Value("${zyplayer.data.admin.datasource.url}")
		private String url;
		@Value("${zyplayer.data.admin.datasource.username}")
		private String username;
		@Value("${zyplayer.data.admin.datasource.password}")
		private String password;
		@Resource
		private PaginationInterceptor paginationInterceptor;
		
		@Bean(name = "manageDatasource")
		public DataSource manageDatasource() {
			DbDatasource dataSource = new DbDatasource();
			dataSource.setId(0L);
			dataSource.setSourceUrl(url);
			dataSource.setSourceName(username);
			dataSource.setSourcePassword(password);
			dataSource.setDriverClassName(driverClassName);
			DatabaseFactoryBean databaseFactoryBean = DatasourceUtil.createDatabaseFactoryBean(dataSource);
			if (databaseFactoryBean == null) {
				throw new RuntimeException("创建数据源失败");
			}
			return databaseFactoryBean.getDataSource();
		}
		
		@Bean(name = "manageSqlSessionFactory")
		public MybatisSqlSessionFactoryBean manageSqlSessionFactory() throws Exception {
			MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
			sqlSessionFactoryBean.setDataSource(manageDatasource());
			sqlSessionFactoryBean.setPlugins(SQL_LOG_INTERCEPTOR, paginationInterceptor);
			
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/mapper/manage/*Mapper.xml"));
			return sqlSessionFactoryBean;
		}
	}
	
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
}
