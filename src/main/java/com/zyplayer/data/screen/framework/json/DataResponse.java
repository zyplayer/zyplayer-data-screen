package com.zyplayer.data.screen.framework.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * 数据库文档返回数据格式
 *
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
public class DataResponse implements ResponseJson {
	private static SerializeConfig mapping = new SerializeConfig();
	
	static {
		mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
	}
	
	private Integer errCode;
	private Long total;
	private String errMsg;
	private Object data;
	
	public DataResponse() {
		this.errCode = 200;
	}
	
	public DataResponse(Object data) {
		this.setData(data);
		this.errCode = 200;
	}
	
	public DataResponse(int errCode, String errMsg) {
		super();
		this.errCode = errCode;
		this.errMsg = errMsg;
	}
	
	public DataResponse(int errCode, String errMsg, Object data) {
		super();
		this.setData(data);
		this.errCode = errCode;
		this.errMsg = errMsg;
	}
	
	public DataResponse(Integer errCode) {
		super();
		this.errCode = errCode;
	}
	
	public Integer getErrCode() {
		return errCode;
	}
	
	public void setErrCode(Integer errCode) {
		this.errCode = errCode;
	}
	
	public String getErrMsg() {
		return errMsg;
	}
	
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		if (data instanceof Page) {
			Page page = (Page) data;
			this.total = page.getTotal();
			this.data = page.getRecords();
		} else {
			this.data = data;
		}
	}
	
	/**
	 * 提示语
	 *
	 * @param errMsg 提示信息
	 * @return 对象
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 */
	public static DataResponse warn(String errMsg) {
		return new DataResponse(300, errMsg);
	}
	
	/**
	 * 错误
	 *
	 * @param errMsg 错误信息
	 * @return 对象
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 */
	public static DataResponse error(String errMsg) {
		return new DataResponse(500, errMsg);
	}
	
	/**
	 * 错误
	 *
	 * @param errCode 错误码
	 * @param errMsg 错误信息
	 * @return 对象
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 */
	public static DataResponse error(int errCode, String errMsg) {
		return new DataResponse(errCode, errMsg);
	}
	
	/**
	 * 成功的返回方法
	 *
	 * @return 对象
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 */
	public static DataResponse ok() {
		return new DataResponse();
	}
	
	/**
	 * 成功的返回方法
	 *
	 * @param data 数据
	 * @return 对象
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 */
	public static DataResponse ok(Object data) {
		return new DataResponse(data);
	}
	
	public String toJson() {
		return JSON.toJSONString(this, mapping);
	}
	
	public void send(HttpServletResponse response) {
		try {
			response.setStatus(200);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Cache-Control", "no-cache, must-revalidate");
			response.getWriter().write(toJson());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return "DefaultResponseJson [errCode=" + errCode + ", errMsg=" + errMsg + ", data=" + data + "]";
	}
	
	public Long getTotal() {
		return total;
	}
	
	public void setTotal(Long total) {
		this.total = total;
	}
}
