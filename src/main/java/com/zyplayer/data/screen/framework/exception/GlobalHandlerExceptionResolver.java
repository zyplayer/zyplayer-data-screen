package com.zyplayer.data.screen.framework.exception;

import com.alibaba.fastjson.JSON;
import com.zyplayer.data.screen.framework.json.DocResponseJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 全局异常处理器
 */
@Component
public class GlobalHandlerExceptionResolver extends SimpleMappingExceptionResolver {
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalHandlerExceptionResolver.class);
	
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		LOGGER.error("---自定义异常处理---", ex);
		request.setAttribute("throwable", ex);
		ModelAndView mv = new ModelAndView();
		response.setStatus(HttpStatus.OK.value());// 设置状态码
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);// 设置ContentType
		response.setCharacterEncoding("UTF-8");// 避免乱码
		response.setHeader("Cache-Control", "no-cache, must-revalidate");
		DocResponseJson responseJson;
		if (ex instanceof ConfirmException) {// 提示性异常
			responseJson = DocResponseJson.error(ex.getMessage());
		} else {// 其他异常
			responseJson = DocResponseJson.error("系统错误");
		}
		boolean isResponseBody = isResponseBody(handler);// 是否返回body
		// 返回页面或者返回内容处理
		if (!isResponseBody) {
			mv.addObject("errJson", responseJson);
			mv.setViewName("/statics/common/500.html");
		} else {
			try {
				String jsonStr = JSON.toJSONString(responseJson);
				response.getWriter().write(jsonStr);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return mv;
	}

	/**
	 * 是否返回body
	 * @author
	 * @since 2017年5月11日
	 * @param handler
	 * @return
	 */
	private boolean isResponseBody(Object handler) {
		if (handler instanceof HandlerMethod) {
			HandlerMethod method = (HandlerMethod) handler;
			ResponseBody body = method.getMethodAnnotation(ResponseBody.class);
			if (body == null) {
				RestController restController = method.getMethod().getDeclaringClass().getAnnotation(RestController.class);
				if (restController == null) {
					return method.getMethod().getReturnType().isAssignableFrom(DocResponseJson.class);
				}
			}
		}
		return true;
	}
	
}

