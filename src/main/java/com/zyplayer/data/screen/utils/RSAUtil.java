package com.zyplayer.data.screen.utils;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.alibaba.fastjson.util.TypeUtils;

import java.util.Date;
import java.util.Objects;

public class RSAUtil {
	private static final String ENCRYPT_COMPARE_STR = "datascreen.zyplayer.com";
	
	/**
	 * ESA解密对比
	 *
	 * @param privateKey 私钥
	 * @param data       加密后数据
	 * @return 解密后的字符串
	 */
	public static boolean decryptCompare(String privateKey, String data) {
		try {
			String[] decryptStr = new RSA(privateKey, null).decryptStr(data, KeyType.PrivateKey).split(";");
			if (!Objects.equals(decryptStr[0], ENCRYPT_COMPARE_STR)) {
				return false;
			}
			Long decryptTime = TypeUtils.castToLong(decryptStr[1]);
			// token的时间加一天，如果比当前时间小说明过期了
			int compareTo = DateTime.of(decryptTime).offset(DateField.DAY_OF_YEAR, 1).compareTo(new Date());
			return compareTo > 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
}
