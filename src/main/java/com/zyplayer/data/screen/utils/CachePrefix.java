package com.zyplayer.data.screen.utils;

public class CachePrefix {
	public static final String WIKI_LOCK_PAGE = "WIKI_LOCK_PAGE_";
	public static final String DB_EDITOR_DATA_CACHE = "DB_EDITOR_DATA_CACHE_";
	
	public static final String USER_ID_TOKEN_CACHE = "USER_ID_TOKEN_CACHE_";
}
