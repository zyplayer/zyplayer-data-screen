package com.zyplayer.data.screen.service.manage;

import com.zyplayer.data.screen.repository.manage.entity.ScreenComponent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 大屏组件表 服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
public interface ScreenComponentService extends IService<ScreenComponent> {

}
