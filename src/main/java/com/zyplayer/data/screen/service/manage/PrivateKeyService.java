package com.zyplayer.data.screen.service.manage;

import com.zyplayer.data.screen.repository.manage.entity.PrivateKey;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-04-13
 */
public interface PrivateKeyService extends IService<PrivateKey> {
	
	void validateSecureParam();
}
