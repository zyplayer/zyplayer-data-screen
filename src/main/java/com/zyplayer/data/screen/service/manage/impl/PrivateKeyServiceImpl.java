package com.zyplayer.data.screen.service.manage.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyplayer.data.screen.framework.exception.ConfirmException;
import com.zyplayer.data.screen.repository.manage.entity.PrivateKey;
import com.zyplayer.data.screen.repository.manage.mapper.PrivateKeyMapper;
import com.zyplayer.data.screen.service.manage.PrivateKeyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyplayer.data.screen.utils.RSAUtil;
import com.zyplayer.data.screen.utils.ThreadLocalUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-04-13
 */
@Service
public class PrivateKeyServiceImpl extends ServiceImpl<PrivateKeyMapper, PrivateKey> implements PrivateKeyService {
	
	@Override
	public void validateSecureParam() {
		QueryWrapper<PrivateKey> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("yn", 1);
		PrivateKey privateKey = this.getOne(queryWrapper);
		if (privateKey == null) {
			throw new ConfirmException("请先访问 http://datascreen.zyplayer.com 获取私钥");
		}
		String customToken = ThreadLocalUtil.getHttpServletRequest().getParameter("customToken");
		if (!RSAUtil.decryptCompare(privateKey.getPrivateKey(), customToken)) {
			throw new ConfirmException("请求参数解密校验失败");
		}
	}
}
