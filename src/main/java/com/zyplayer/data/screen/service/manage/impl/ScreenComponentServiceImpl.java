package com.zyplayer.data.screen.service.manage.impl;

import com.zyplayer.data.screen.repository.manage.entity.ScreenComponent;
import com.zyplayer.data.screen.repository.manage.mapper.ScreenComponentMapper;
import com.zyplayer.data.screen.service.manage.ScreenComponentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 大屏组件表 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
@Service
public class ScreenComponentServiceImpl extends ServiceImpl<ScreenComponentMapper, ScreenComponent> implements ScreenComponentService {

}
