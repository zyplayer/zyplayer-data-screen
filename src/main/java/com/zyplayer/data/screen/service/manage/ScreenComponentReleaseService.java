package com.zyplayer.data.screen.service.manage;

import com.zyplayer.data.screen.repository.manage.entity.ScreenComponentRelease;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 发布后的大屏组件表 服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
public interface ScreenComponentReleaseService extends IService<ScreenComponentRelease> {

}
