package com.zyplayer.data.screen.service.manage.impl;

import com.zyplayer.data.screen.repository.manage.entity.ScreenComponentRelease;
import com.zyplayer.data.screen.repository.manage.mapper.ScreenComponentReleaseMapper;
import com.zyplayer.data.screen.service.manage.ScreenComponentReleaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发布后的大屏组件表 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
@Service
public class ScreenComponentReleaseServiceImpl extends ServiceImpl<ScreenComponentReleaseMapper, ScreenComponentRelease> implements ScreenComponentReleaseService {

}
