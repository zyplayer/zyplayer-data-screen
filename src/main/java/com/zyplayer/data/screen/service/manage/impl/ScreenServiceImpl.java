package com.zyplayer.data.screen.service.manage.impl;

import com.zyplayer.data.screen.repository.manage.entity.Screen;
import com.zyplayer.data.screen.repository.manage.mapper.ScreenMapper;
import com.zyplayer.data.screen.service.manage.ScreenService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 大屏表 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-04-13
 */
@Service
public class ScreenServiceImpl extends ServiceImpl<ScreenMapper, Screen> implements ScreenService {

}
