package com.zyplayer.data.screen.service.manage.impl;

import com.zyplayer.data.screen.repository.manage.entity.DbDatasource;
import com.zyplayer.data.screen.repository.manage.mapper.DbDatasourceMapper;
import com.zyplayer.data.screen.service.manage.DbDatasourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
@Service
public class DbDatasourceServiceImpl extends ServiceImpl<DbDatasourceMapper, DbDatasource> implements DbDatasourceService {

}
