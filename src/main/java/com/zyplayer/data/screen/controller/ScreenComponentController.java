package com.zyplayer.data.screen.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyplayer.data.screen.framework.exception.ConfirmException;
import com.zyplayer.data.screen.framework.json.DataResponse;
import com.zyplayer.data.screen.framework.json.ResponseJson;
import com.zyplayer.data.screen.repository.manage.entity.Screen;
import com.zyplayer.data.screen.repository.manage.entity.ScreenComponent;
import com.zyplayer.data.screen.service.manage.PrivateKeyService;
import com.zyplayer.data.screen.service.manage.ScreenComponentService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 大屏组件控制器
 *
 * @author 暮光：城中城
 * @since 2019年12月01日
 */
@RestController
@RequestMapping("/screenComponent")
public class ScreenComponentController {
	
	@Resource
	ScreenComponentService screenComponentService;
	@Resource
	PrivateKeyService privateKeyService;
	
	@PostMapping(value = "/updateAll")
	public ResponseJson updateAll(String component) {
		privateKeyService.validateSecureParam();
		Set<Long> screenIds = new HashSet<>();
		List<ScreenComponent> componentList = JSON.parseArray(component, ScreenComponent.class);
		for (ScreenComponent screenComponent : componentList) {
			if (StringUtils.isBlank(screenComponent.getTitle())) {
				return DataResponse.warn("标题必填");
			} else if (StringUtils.isBlank(screenComponent.getType())) {
				return DataResponse.warn("组件类型必填");
			}
			screenIds.add(screenComponent.getScreenId());
		}
		if (screenIds.size() > 1) {
			return DataResponse.warn("只能更新一个大屏的组件");
		} else if (screenIds.size() <= 0) {
			return DataResponse.warn("必须指定所属大屏");
		}
		Long screenId = screenIds.stream().findFirst().orElse(0L);
		for (ScreenComponent screenComponent : componentList) {
			screenComponent.setScreenId(screenId);
			this.saveOrUpdate(screenComponent);
		}
		return DataResponse.ok();
	}
	
	@PostMapping(value = "/update")
	public ResponseJson update(ScreenComponent screenComponent, Integer delFlag) {
		privateKeyService.validateSecureParam();
		if (Objects.equals(delFlag, 1)) {
			boolean remove = screenComponentService.removeById(screenComponent.getId());
			if (!remove) {
				return DataResponse.warn("自建服务组件删除失败");
			}
		} else {
			this.saveOrUpdate(screenComponent);
		}
		return DataResponse.ok(screenComponent);
	}
	
	private void saveOrUpdate(ScreenComponent screenComponent) {
		Long sourceId = Optional.ofNullable(screenComponent.getId()).orElse(0L);
		if (sourceId <= 0) {
			throw new ConfirmException("数据ID不能为空");
		}
		// 更新
		screenComponent.setUpdateTime(new Date());
		boolean isUpdate = screenComponentService.updateById(screenComponent);
		if (!isUpdate) {
			// 新增
			screenComponent.setCreationTime(new Date());
			screenComponent.setUpdateTime(new Date());
			screenComponentService.save(screenComponent);
		}
	}
}

