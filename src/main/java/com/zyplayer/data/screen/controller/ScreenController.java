package com.zyplayer.data.screen.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.dozermapper.core.Mapper;
import com.zyplayer.data.screen.framework.json.DataResponse;
import com.zyplayer.data.screen.framework.json.ResponseJson;
import com.zyplayer.data.screen.repository.manage.entity.Screen;
import com.zyplayer.data.screen.repository.manage.entity.ScreenComponentRelease;
import com.zyplayer.data.screen.service.manage.PrivateKeyService;
import com.zyplayer.data.screen.service.manage.ScreenComponentReleaseService;
import com.zyplayer.data.screen.service.manage.ScreenComponentService;
import com.zyplayer.data.screen.service.manage.ScreenService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 大屏控制器
 *
 * @author 暮光：城中城
 * @since 2019年12月01日
 */
@RestController
@RequestMapping("/screen")
public class ScreenController {
	
	@Resource
	Mapper mapper;
	@Resource
	ScreenComponentService screenComponentService;
	@Resource
	ScreenComponentReleaseService screenComponentReleaseService;
	@Resource
	PrivateKeyService privateKeyService;
	@Resource
	ScreenService screenService;
	
	@PostMapping(value = "/release")
	public ResponseJson release(Long id, Long deleteScreenReleaseId, Integer viewType, String viewPassword, String component) {
		privateKeyService.validateSecureParam();
		// 保存大屏信息
		Screen screen = new Screen();
		screen.setViewPassword(viewPassword);
		screen.setViewType(viewType);
		screen.setScreenId(id);
		screenService.saveOrUpdate(screen);
		// 保存屏幕组件
		List<ScreenComponentRelease> componentReleaseList = JSON.parseArray(component, ScreenComponentRelease.class);
		screenComponentReleaseService.saveBatch(componentReleaseList);
		// 删除旧的大屏信息
		QueryWrapper<Screen> screenWrapper = new QueryWrapper<>();
		screenWrapper.eq("screen_id", deleteScreenReleaseId);
		screenService.remove(screenWrapper);
		// 删除之前的组件配置
		QueryWrapper<ScreenComponentRelease> wrapperComponentRelease = new QueryWrapper<>();
		wrapperComponentRelease.eq("screen_id", deleteScreenReleaseId);
		screenComponentReleaseService.remove(wrapperComponentRelease);
		return DataResponse.ok();
	}
	
//	@PostMapping(value = "/copyScreen")
//	public ResponseJson copyScreen(Long id, Long copyFrom) {
//		privateKeyService.validateSecureParam();
//		// 拷贝组件
//		QueryWrapper<ScreenComponent> wrapperComponent = new QueryWrapper<>();
//		wrapperComponent.eq("screen_id", copyFrom);
//		List<ScreenComponent> componentList = screenComponentService.list(wrapperComponent);
//		if (CollectionUtils.isNotEmpty(componentList)) {
//			List<ScreenComponent> componentCopyList = new LinkedList<>();
//			for (ScreenComponent screenComponent : componentList) {
//				screenComponent.setId(null);
//				screenComponent.setScreenId(id);
//				screenComponent.setCreationTime(new Date());
//				screenComponent.setUpdateTime(new Date());
//				componentCopyList.add(screenComponent);
//			}
//			screenComponentService.saveBatch(componentCopyList);
//		}
//		return DataResponse.ok();
//	}
}

