package com.zyplayer.data.screen.repository.manage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 大屏表
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-04-13
 */
public class Screen implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 大屏id
     */
    private Long screenId;

    /**
     * 查看方式 0=随便看 1=需要密码
     */
    private Integer viewType;

    /**
     * 访问密码
     */
    private String viewPassword;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getScreenId() {
        return screenId;
    }

    public void setScreenId(Long screenId) {
        this.screenId = screenId;
    }
    public Integer getViewType() {
        return viewType;
    }

    public void setViewType(Integer viewType) {
        this.viewType = viewType;
    }
    public String getViewPassword() {
        return viewPassword;
    }

    public void setViewPassword(String viewPassword) {
        this.viewPassword = viewPassword;
    }

    @Override
    public String toString() {
        return "Screen{" +
            "id=" + id +
            ", screenId=" + screenId +
            ", viewType=" + viewType +
            ", viewPassword=" + viewPassword +
        "}";
    }
}
