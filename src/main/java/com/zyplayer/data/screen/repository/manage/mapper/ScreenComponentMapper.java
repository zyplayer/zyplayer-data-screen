package com.zyplayer.data.screen.repository.manage.mapper;

import com.zyplayer.data.screen.repository.manage.entity.ScreenComponent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 大屏组件表 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
public interface ScreenComponentMapper extends BaseMapper<ScreenComponent> {

}
