package com.zyplayer.data.screen.repository.manage.mapper;

import com.zyplayer.data.screen.repository.manage.entity.PrivateKey;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-04-13
 */
public interface PrivateKeyMapper extends BaseMapper<PrivateKey> {

}
