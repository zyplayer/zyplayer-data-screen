package com.zyplayer.data.screen.repository.manage.mapper;

import com.zyplayer.data.screen.repository.manage.entity.Screen;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 大屏表 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-04-13
 */
public interface ScreenMapper extends BaseMapper<Screen> {

}
