package com.zyplayer.data.screen.repository.manage.mapper;

import com.zyplayer.data.screen.repository.manage.entity.DbDatasource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
public interface DbDatasourceMapper extends BaseMapper<DbDatasource> {

}
