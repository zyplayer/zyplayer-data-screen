package com.zyplayer.data.screen.repository.manage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 大屏组件表
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-03-17
 */
public class ScreenComponent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;

    /**
     * 大屏id
     */
    private Long screenId;

    /**
     * 组件名称
     */
    private String title;

    /**
     * 组件类型
     */
    private String type;

    /**
     * 组件类型 mysql
     */
    private String dataType;

    /**
     * 组件数据查询SQL
     */
    private String dataSql;

    /**
     * 数据源ID
     */
    private Long datasourceId;

    /**
     * 图表配置ID
     */
    private Long chartOptionId;

    /**
     * 创建时间
     */
    private Date creationTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 请求数据的URL
     */
    private String apiUrl;

    /**
     * 请求数据的数据位置
     */
    private String apiDataPlace;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getScreenId() {
        return screenId;
    }

    public void setScreenId(Long screenId) {
        this.screenId = screenId;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
    public String getDataSql() {
        return dataSql;
    }

    public void setDataSql(String dataSql) {
        this.dataSql = dataSql;
    }
    public Long getDatasourceId() {
        return datasourceId;
    }

    public void setDatasourceId(Long datasourceId) {
        this.datasourceId = datasourceId;
    }
    public Long getChartOptionId() {
        return chartOptionId;
    }

    public void setChartOptionId(Long chartOptionId) {
        this.chartOptionId = chartOptionId;
    }
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ScreenComponent{" +
            "id=" + id +
            ", screenId=" + screenId +
            ", title=" + title +
            ", type=" + type +
            ", dataType=" + dataType +
            ", dataSql=" + dataSql +
            ", datasourceId=" + datasourceId +
            ", chartOptionId=" + chartOptionId +
            ", creationTime=" + creationTime +
            ", updateTime=" + updateTime +
        "}";
    }
    
    public String getApiUrl() {
        return apiUrl;
    }
    
    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
    
    public String getApiDataPlace() {
        return apiDataPlace;
    }
    
    public void setApiDataPlace(String apiDataPlace) {
        this.apiDataPlace = apiDataPlace;
    }
}
